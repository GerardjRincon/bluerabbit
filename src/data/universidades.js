const universidades = [
    {
        id:1,
        nombre:'UBA Derecho',
        img: require('../../assets/img/UBA.jpeg')
    },
    {
        id:2,
        nombre:'UADE',
        img: require('../../assets/img/UADE.jpeg')
    },
    {
        id:3,
        nombre:'UBA Medicina',
        img: require('../../assets/img/UBAMedicina.jpeg')
    },
    {
        id:4,
        nombre:'UNC',
        img: require('../../assets/img/UNC.jpeg')
    },
    {
        id:5,
        nombre:'UNMDP',
        img: require('../../assets/img/UNMDP.jpeg')
    },
    {
        id:6,
        nombre:'UNT',
        img: require('../../assets/img/UNT.jpeg')
    },
    {
        id:7,
        nombre:'UNICEN',
        img: require('../../assets/img/UNICEN.jpeg')
    },
    {
        id:8,
        nombre:'UNS',
        img: require('../../assets/img/UNS.jpeg')
    },
    {
        id:9,
        nombre:'UNR',
        img: require('../../assets/img/UNR.jpeg')

    },
    {
        id:10,
        nombre:'UNQ',
        img: require('../../assets/img/UNQ.jpeg')

    },
]

export default universidades;